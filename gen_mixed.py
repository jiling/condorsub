import numpy as np
f = open("./Mixed_scan.txt", "w")
# B field max
b = np.linspace(0.27,0.28,6)
# tws displacement 
x = np.linspace(0,4,17)
phases = np.linspace(31,37,7)
charges = np.linspace(0.5,1,6)
# print(charges)
# print(b)
# print(x)
# print(phases)
# print(-(90-phases))
for c in charges:
    for x_off in x:
        for B_field in b:
            for phi in phases:
                    f.write("{0:.1f} {1:.3f} {2:.2f}e-3 {3:d} {4:d}\n".format(c, B_field, x_off, int(phi), -(90-int(phi))))
                    # print("{0:.1f} {1:.2f} {2:.2f}e-3 {3:d} {4:d}\n".format(c, B_field, x_off, int(phi), -(90-int(phi))))
f.close()
