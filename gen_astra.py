#!/usr/bin/env python
import sys
import subprocess
import os
import glob
import random
import tarfile
tar = tarfile.open("job.tar")
tar.extractall()
tar.close()

awake_rfgun_in = """
&NEWRUN
 Head='A-1-3D'
 RUN=1
 Distribution = '<gun_ini>',	Xoff=<cathod_X_off>,	Yoff=<cathod_Y_off>
 Track_All=T,			Auto_Phase=F
! Phase_Scan = T
 H_Max=0.001,			H_Min=0.0,	Max_Step=1000000000
! Lmonitor=T
/




&OUTPUT
 ZSTART=0.0,		ZSTOP=1.600
 Zemit=800,       	Zphase=2
 RefS=F,			T_PhaseS=F
 Lsub_Rot=F
 EmitS=T,			Tr_EmitS=T,		PhaseS=T

 Screen(1) = 1.520
/




&CHARGE
 LSPCH=T,			LSPCH3D=F,		Lmirror=T
 Nrad=30, 			Cell_Var=2.0,		Nlong_In=60
 Min_Grid=0.0
 Max_Scale=0.01
/



&SOLENOID
 LBField=T,			File_Bfield(1)='modified_solenoid.txt',		MaxB(1)=<MaxB>,		S_Pos(1)=0.0
/


&Cavity
LEfield=T

File_Efield(1) = 'Fields/Gun/Phin3.txt',	    Nue(1)=2.998,		MaxE(1)=79.6,  			C_Pos(1)=0.0,		    Phi(1)=218.59
/
"""
awake_tws_in="""
&NEWRUN
 Head='A-1-3D'
 RUN=1
 Distribution = 'awake_rfgun_<suffix>.0160.001',	Xoff=0.0,	Yoff=0.0
 Track_All=T,			Auto_Phase=F
! Phase_Scan = T
 H_Max=0.001,			H_Min=0.0,	Max_Step=1000000000
! Lmonitor=T
/




&OUTPUT
 ZSTART=1.60,		ZSTOP=4.45827
 Zemit=500,       	Zphase=2
 RefS=F,			T_PhaseS=F
 Lsub_Rot=F
 EmitS=T,			Tr_EmitS=T,		PhaseS=T

 Screen(1) = 1.657
 Screen(2) = 3.455
 Screen(3) = 4.3217
/




&CHARGE
 LSPCH=T,			LSPCH3D=T,		Lmirror=F
 Nxf = 64, Nx0 = 15, Nyf = 64, Ny0 = 15, Nzf = 64, Nz0 = 15,
 Max_Scale=0.01
/

&CAVITY
LEField=T,
File_Efield(1) = 'Fields/ACC/3D_Acc_F1', Nue(1)=2.998, C_NoScale(1)=T, C_Pos(1)=1.60, Phi(1)=<Phi1>, C_xoff(1) = <tws_x_off>, C_yoff(1) = <tws_y_off>
File_Efield(2) = 'Fields/ACC/3D_Acc_F2', Nue(2)=2.998, C_NoScale(2)=T, C_Pos(2)=1.60, Phi(2)=<Phi2>, C_xoff(2) = <tws_x_off>, C_yoff(2) = <tws_y_off>
/
"""


# x_off = sys.argv[1]
# y_off = sys.argv[2]
# suffix = "x_{0}_y_{1}".format(x_off, y_off)

# MaxB = sys.argv[1]
charge = sys.argv[1]
MaxB = sys.argv[2]
x_off = sys.argv[3]
# 34 and -56 are default
Phi1 = sys.argv[4]
Phi2 = sys.argv[5]
suffix = "ChargeMult_{0}_MaxB_{1}_Xoff_{2}_Phase_{3}_{4}".format(charge, MaxB, x_off, Phi1, Phi2)
gun_in = open("./gun_5k.ini", 'r').readlines()
nominal_macro_charge = "-27.9084e-06"
gun_in = [i.replace(nominal_macro_charge, str(float(charge) * float(nominal_macro_charge))) for i in gun_in]
gun_out = open("./gun.ini", 'w')
for i in gun_in:
    gun_out.write(i)
gun_out.close()

gun_ini = "gun.ini"
awake_rfgun_path = "awake_rfgun_{0}.in".format(suffix)
# awake_rfgun_in = awake_rfgun_in.replace("<cathod_X_off>", x_off).replace("<cathod_Y_off>", y_off).replace("<gun_ini>", gun_ini)
awake_rfgun_in = awake_rfgun_in.replace("<cathod_X_off>", "0.0").replace("<cathod_Y_off>", "0.0").replace("<gun_ini>", gun_ini)
awake_rfgun_in = awake_rfgun_in.replace("<MaxB>", MaxB)
# awake_rfgun_in = awake_rfgun_in.replace("<MaxB>", "0.270")
awake_rfgun_file = open(awake_rfgun_path, "w")
awake_rfgun_file.write(awake_rfgun_in)
awake_rfgun_file.close()

awake_tws_path = "awake_tws_{0}.in".format(suffix)
awake_tws_in = awake_tws_in.replace("<suffix>", suffix).replace("<tws_x_off>", x_off).replace("<tws_y_off>", "0.0")
awake_tws_in = awake_tws_in.replace("<suffix>", suffix).replace("<Phi1>", Phi1).replace("<Phi2>", Phi2)
awake_tws_file = open(awake_tws_path, "w")
awake_tws_file.write(awake_tws_in)
awake_tws_file.close()

os.system("./Astra "+ awake_rfgun_path)
os.system("./Astra "+ awake_tws_path)
os.system("mv *001 " + "/eos/user/j/jiling/condor/AWAKE/Mixed_scan/")
